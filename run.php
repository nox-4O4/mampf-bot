<?php

	use MampfBot\Controller\Server;
	use MampfBot\View\LogFactory;
	use Monolog\Logger;

	require "vendor/autoload.php";
	require "Configuration.php";

	if(php_sapi_name() != 'cli')
		exit;

	try {
		cli_set_process_title("MampfBot v" . VERSION);

		LogFactory::get()->log(Logger::INFO, "MampfBot started");

		while(true)
			Server::getInstance()
			      ->queryMampf()
			      ->requestAPI();
	} catch(\Throwable $t) {
		try {
			LogFactory::get()->log(Logger::CRITICAL, "Unhandled exception: {$t->getMessage()}");
		} catch(\Throwable $tt) {
			echo "Unhandled exception: {$t->getMessage()}\n{$t->getTraceAsString()}";
		}
	}

