<?php

	namespace MampfBot\Controller;

	use MampfBot\Model\Mampf\MampfFactory;
	use MampfBot\Traits\Singleton;

	class Server {
		use Singleton;

		/**
		 * Checks if a new Mampf is available
		 *
		 * @return Server
		 */
		public function queryMampf(): Server {
			$mampfs = MampfFactory::fetch();

			// TODO

			return $this;
		}

		/**
		 * Initiates communication with Telegram API
		 *
		 * @return Server
		 */
		public function requestAPI(): Server {
			// TODO

			return $this;
		}
	}
